<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;

use DataTables;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $vehicles = Vehicle::all();
            return Datatables::of($vehicles)
                ->addIndexColumn()
                ->addColumn('action',function($row){
                    $btn = '';
                    if($row->is_agidrive !==NULL){
                        $btn = '<a href="'.route('agi-log.count', [$row->id]).'" data-toggle="tooltip" data-original-title="Log Count" class="btn btn-primary btn-sm me-1">Log Count</a>';
                        $btn .= '<a href="'.route('agi-log.last.info', [$row->id]).'" data-toggle="tooltip" data-original-title="Last Info" class="btn btn-info btn-sm">Last Info</a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])->make(true);
        }
        return view('vehicles.index');
    }
}
