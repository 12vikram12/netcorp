# Laravel HIPL 
Develop By [Ram Kumawat](https://www.helpfulinsightsolution.com/)


What's inside:

- Core Laravel Setup
- Manage the migration for new tables 
- Manage the seeder for exist table sql files
- User the Microsoft bingmaps api for get the address 
- User the Google map api to show the address


---

## How to use

- Clone the repository with _git clone_
- Copy _.env.example_ file to _.env_ and edit database credentials there
- Run _composer install_
- Run _php artisan key:generate_
- Run _php artisan migrate --seed_ (it has some seeded data for your testing and upload exist sql files)
- That's it: launch the main URL. 


## License
Basically, feel free to use and re-use any way you want.

---

## More from our HIPL Team
- Check out our Website [Helpful Insight Pvt. Ltd.](https://www.helpfulinsightsolution.com/)