@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="col-lg-12 margin-tb">
    <div class="pull-left">
        <h3>Active Vehicle List</h3>
    </div>
</div>
<table class="table table-hover table-bordered data-table text-center">
    <thead class="bg-secondary text-white" >
        <tr>
            <th width="80px">ID</th>
            <th >Name</th>
            <th >AgiDrive</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function () {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                order: [1, "asc"], //or asc 
                ajax: "{{ route('vehicles.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'is_agidrive', name: 'is_agidrive'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
        });
    </script>
@endsection