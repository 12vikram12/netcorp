@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="col-lg-12 margin-tb">
    <div class="pull-left">
        <h3>Log Count</h3>
    </div>
</div>
<table class="table table-hover table-bordered data-table text-center">
    <thead class="bg-secondary text-white" >
        <tr>
            <th width="80px">No</th>
            <th>Name</th>
            <th>Year-Month</th>
            <th>Count</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function () {
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('agi-log.count', $id) }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'vehicle.name', name: 'vehicle.name'},
                    {data: 'local_time', name: 'local_time'},
                    {data: 'log_count', name: 'log_count'},
                ]
            });
            
        });
    </script>
@endsection